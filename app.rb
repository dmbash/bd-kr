require 'sinatra/base'
require 'pg'
require 'sinatra/static_assets'
require 'sinatra/url_for'

$connection = PG.connect(dbname:'networking', user:'db_admin', password:'123', port:5433)

class AccountCreation < Sinatra::Base
    enable :sessions

    get '/login' do
        erb :login, :locals => {:message => nil}
    end
    
    post '/login' do
        login = params[:login]
        password = params[:password]
        
        query = "SELECT * FROM ACCOUNTS WHERE LOGIN = $1"
        
        res = $connection.exec_params(query, [login])
        if res.cmdtuples() == 0     
            erb :login, :locals => {:message => "Account with specified login not found"}
        else
            account_password = res.getvalue(0,1).strip!
            if password == account_password
                session[:login] = login
                redirect "/"
            else
                erb :login, :locals => {:message => "Incorrect password!"}
            end
        end
    end

    get '/register' do
        erb :register, :locals => {:message => nil}
    end
    
    post '/register' do
        login = params[:login]
        password = params[:password]
        query = "SELECT NEW_ACCOUNT($1, $2)"
        new_account = $connection.exec_params(query, [login, password])
        erb :register, :locals => {:message => "Success"}
        rescue PG::Error => e
            erb :register, :locals => {:message => e.message}
    end
    
end

class ProfileCreation < Sinatra::Base
    get '/profile_creation' do
        erb :profile_creation, :locals => {:error => nil}
    end
    
    post '/profile_creation' do
        name = params[:name]
        age = params[:age]
        country = params[:country]
        city = params[:city]
        query = "INSERT INTO PROFILES(PROFILE_NAME, PROFILE_AGE, PROFILE_LOGIN, PROFILE_CITY, PROFILE_COUNTRY) VALUES ($1, $2, $3, $4, $5)"
        begin
        res = $connection.exec_params(query, [name, age, session[:login], city, country])
        rescue PG::Error => e
            erb :profile_creation, :locals => {:error => e.message}
        end
        redirect "/"
    end
end

class App < Sinatra::Base
    helpers Sinatra::UrlForHelper
    set :public_folder, File.dirname(__FILE__) + '/public'
    use AccountCreation
    use ProfileCreation
    
    before do
        unless session[:login]
            redirect "/login"
        end
    end

    before do
        unless $connection.exec_params("SELECT * FROM PROFILES WHERE PROFILE_LOGIN = $1", [session[:login]]).cmdtuples == 1
            redirect "/profile_creation"
        end
    end

    get '/' do
        profiles = []
        query = "SELECT * FROM PROFILES WHERE PROFILE_LOGIN != $1"
        res = $connection.exec_params(query, [session[:login]])
        res.each do |row|
            name = row['profile_name'].strip!
            age = row['profile_age']
            city = row['profile_city'].strip!
            country = row['profile_country'].strip!
            login = row['profile_login'].strip!
            occupations = $connection.exec_params("SELECT GET_OCCUPATIONS($1)", [login])
            occupations = nil if occupations.cmdtuples == 0
            profile = {:name => name, :age => age, :city => city, :country => country, :login => login, :occupations => occupations}
            profiles.append(profile)
        end
        
        erb :index, :locals => {:profiles => profiles, :error => nil}
    end
    
    get '/profile/:login' do
        query = "SELECT * FROM PROFILES WHERE PROFILE_LOGIN = $1"
        res = $connection.exec_params(query, [params[:login]])
        name = res[0]['profile_name'].strip!
        age = res[0]['profile_age']
        city = res[0]['profile_city'].strip!
        country = res[0]['profile_country'].strip!
        login = res[0]['profile_login'].strip!
        info = {:name => name, :age => age, :city => city, :country => country}
        query = "SELECT * FROM PROFILE_GOALS WHERE PROFILE_LOGIN = $1"
        goals = $connection.exec_params(query, [params[:login]])
        goals = nil if goals.cmdtuples == 0
        query = "SELECT GET_OCCUPATIONS($1)"
        occupancies = $connection.exec_params(query, [params[:login]])
        occupancies = nil if occupancies.cmdtuples == 0
        erb :profile, :locals => {:error => nil, :info => info, :occupancies => occupancies, :goals => goals, :login => login}
    end

    get '/new_goal' do
        erb :new_goal, :locals => {:error => nil}
    end

    get '/start_dialog/:login' do
        user = params[:login]
        query = "SELECT NEW_DIALOG($1, $2)"
        begin
            $connection.exec_params(query, [session[:login], user])
        rescue PG::Error => e
            erb :index, :locals => {:error => e.message}
        end
        redirect '/dialogs'
    end

    post '/new_goal' do
        goal = params['goal']
        query = "INSERT INTO PROFILE_GOALS(GOAL, PROFILE_LOGIN) VALUES ($1, $2)"
        begin
        $connection.exec_params(query, [goal, session[:login]])
        rescue PG::Error => e
            erb :new_goal, :locals => {:error => e.message}
        end
        redirect "/profile/#{session[:login]}"
    end

    post '/delete_goal/:goal' do
        goal = params['goal']
        query = "DELETE FROM PROFILE_GOALS WHERE PROFILE_LOGIN = $1 AND GOAL = $2"
        $connection.exec_params(query, [session[:login], goal])
        redirect "/profile/#{session[:login]}"
    end

    delete '/delete_goal/:goal' do
        redirect "/profile/#{session[:login]}"
    end

    get '/new_profile_occupation' do
        erb :new_profile_occupation, :locals => {:error => nil}
    end

    post '/new_profile_occupation' do
        occupation = params['occupation']
        query = "SELECT ADD_OCCUPATION($1, $2)"
        begin
        $connection.exec_params(query, [session[:login], occupation])
        rescue PG::Error => e
            erb :new_profile_occupation, :locals => {:error => e.message} 
        end
        redirect "/profile/#{session[:login]}"
    end

    post '/delete_profile_occupation/:occupation_name' do
        occupation = params['occupation_name']
        query = "SELECT DELETE_OCCUPATION($1, $2)"
        $connection.exec_params(query, [session[:login], occupation])
        redirect "/profile/#{session[:login]}"
    end

    delete '/delete_profile_occupation/:occupation_name' do
        redirect "/profile/#{session[:login]}"
    end
    
    get '/dialogs' do
        query = "SELECT GET_DIALOGS($1)"
        begin
        dialogs = $connection.exec_params(query, [session[:login]])
        dialogs = nil if dialogs.cmdtuples == 0
        rescue PG::Error => e
            redirect '/'
        end
        erb :dialogs, :locals => {:dialogs => dialogs}
    end
    
    get '/dialog/:id' do
        query = "SELECT * FROM MESSAGES WHERE DIALOG_ID = $1"
        begin
            messages = $connection.exec_params(query, [params[:id]])
            messages = nil if messages.cmdtuples == 0
        rescue PG::Error => e
            redirect '/'
        end
        erb :dialog, :locals => {:messages => messages}
    end
    
    post '/dialog/:id' do
        text = params[:text]
        timestamp = Time.new.strftime("%d-%m-%Y %H:%M:%S")
        author = session[:login]
        dialog_id = params[:id]
        query = "INSERT INTO MESSAGES(SENDER_LOGIN, DIALOG_ID, MESSAGE_TIMESTAMP, MESSAGE_TEXT) VALUES ($1, $2, $3, $4)"
        begin
            message = $connection.exec_params(query, [author, dialog_id, timestamp, text])
        rescue PG::Error => e
            puts e.message
        end
        redirect "/dialog/#{params[:id]}"
    end

    get '/logout' do
        session[:login] = nil
        redirect '/login'
    end

    run! if app_file == $0
end




